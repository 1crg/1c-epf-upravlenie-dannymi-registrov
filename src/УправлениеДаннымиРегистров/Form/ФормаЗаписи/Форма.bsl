﻿
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Заголовок 		 = Параметры.Заголовок;
	ОтображатьФлажок = Параметры.ОтображатьФлажок;
	
	СоздатьЭлементы();
	
КонецПроцедуры

&НаСервере
Процедура СоздатьЭлементы() 
	
	НовыеРеквизиты = Новый Массив;
	
	Для Каждого ОписаниеПоля Из Параметры.Поля Цикл
		
		Имя = "Запись" + ОписаниеПоля.Имя;
		
		НовыйРеквизит = Новый РеквизитФормы(Имя, ОписаниеПоля.ТипЗначения,, ОписаниеПоля.Заголовок);
		НовыеРеквизиты.Добавить(НовыйРеквизит);
		
		НовыйРеквизит = Новый РеквизитФормы("Флажок" + Имя, Новый ОписаниеТипов("Булево"));
		НовыеРеквизиты.Добавить(НовыйРеквизит);
		
		ИменаРеквизитов.Добавить(Имя, ОписаниеПоля.Имя);
		
	КонецЦикла;

	ИзменитьРеквизиты(НовыеРеквизиты);
	
	
	Для Каждого ОписаниеПоля Из Параметры.Поля Цикл
		
		Имя = "Запись" + ОписаниеПоля.Имя;
		
		ЭлементГруппа = Элементы.Добавить("Группа" + Имя, Тип("ГруппаФормы"), ЭтаФорма);
		ЭлементГруппа.Вид 				  = ВидГруппыФормы.ОбычнаяГруппа;
		ЭлементГруппа.Группировка 		  = ГруппировкаПодчиненныхЭлементовФормы.Горизонтальная;
		ЭлементГруппа.Отображение 		  = ОтображениеОбычнойГруппы.Нет;
		ЭлементГруппа.ОтображатьЗаголовок = Ложь;
		
		Если ОтображатьФлажок Тогда
			НовыйЭлемент = Элементы.Добавить("Флажок" + Имя, Тип("ПолеФормы"), ЭлементГруппа);
			НовыйЭлемент.Вид = ВидПоляФормы.ПолеФлажка;
			НовыйЭлемент.ПутьКДанным = "Флажок" + Имя;	
			НовыйЭлемент.ПоложениеЗаголовка = ПоложениеЗаголовкаЭлементаФормы.Нет;
		КонецЕсли;
			
		НовыйЭлемент = Элементы.Добавить(Имя, Тип("ПолеФормы"), ЭлементГруппа);
		НовыйЭлемент.Вид = ВидПоляФормы.ПолеВвода;
		НовыйЭлемент.ПутьКДанным = Имя;
		НовыйЭлемент.УстановитьДействие("ПриИзменении", "ПриИзмененииРеквизита");
		
		Если Параметры.ДанныеЗаписи <> Неопределено Тогда
			ЭтаФорма[Имя] = Параметры.ДанныеЗаписи[ОписаниеПоля.Имя];	
		КонецЕсли;
		
	КонецЦикла;

КонецПроцедуры

&НаКлиенте
Процедура ОК(Команда)
	
	Результат = Новый Структура;
	Для Каждого КлючЗначение Из ИменаРеквизитов Цикл
		Если ОтображатьФлажок 
			И ЭтаФорма["ФлажокЗапись" + КлючЗначение.Представление]
			Или Не ОтображатьФлажок Тогда
			Результат.Вставить(КлючЗначение.Представление, ЭтаФорма[КлючЗначение.Значение]);	
		КонецЕсли;
	КонецЦикла;
	
	Закрыть(Результат);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриИзмененииРеквизита(Элемент)
	ЭтаФорма["Флажок" + Элемент.Имя] = ЗначениеЗаполнено(ЭтаФорма[Элемент.Имя]) Или ЭтаФорма["Флажок" + Элемент.Имя];	
КонецПроцедуры